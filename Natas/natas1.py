#!/usr/bin/python

import requests
import re


def nextuser(actual_user):
	number = int(re.findall("[0-9]+", actual_user)[0]) +1
	return str(actual_user[:5]) + str(number)

user = "natas0"
password = "natas0"

url = f"http://{user}.natas.labs.overthewire.org"

response = requests.get(url, auth=(user,password))
content = response.text
try:
	pswd = re.findall("<!--The password for natas[0-9]+ is (.*) -->", content)[0]
	print(pswd)
except IndexError as e:
	print(f"No results for the password of {nextuser(user)}.")
else:
	pass
finally:
	pass



